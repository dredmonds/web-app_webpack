const dotenv = require('dotenv');
const environment = (process.env.NODE_ENV || 'development').trim();
// load environment variables using dotenv
const {error} = dotenv.config({path: `./.env.${environment}`});

const development = require('./config/webpack.dev.config.js');
const production = require('./config/webpack.prod.config.js');

if(!error && environment === 'development') {
  module.exports = development(process.env);
} else if(!error && environment === 'production') {
  module.exports = production(process.env);
} else if(error) {
  /**
   * NOTE: 
   * Some hosting providers like Heroku, ZEIT Now, and AWS Elastic Beanstalk
   * will populate your environment with variables.
   */
  console.log('EnvConfigStatus', error);
  throw error;
}
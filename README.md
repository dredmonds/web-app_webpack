# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary:
A web application boilerplate using webpack latest version
* Version:
v0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up:
Git pull the project repository
* Configuration:
Install NodeJS, Yarn and Serve(npm install -g server) for localhost web publishing of build folder.
* Dependencies:
Please refer to project package.json
* Database configuration
* How to run tests:
To be follows...
* Deployment instructions:
$yarn start -> for development mode
$yarn build:dev -> for development mode with NODE_ENV=development
$yarn build:prod -> for production mode with NODE_ENV=production

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
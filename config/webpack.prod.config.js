const merge = require('webpack-merge');
const UglifyjsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const webpackBaseConfig = require('./webpack.common.config');

module.exports = (env) => {
  return merge(webpackBaseConfig(env), {
    // mode: 'production',
    optimization: {
      minimizer: [
        new UglifyjsPlugin({
          cache: true,
          parallel: true
        }),
        new OptimizeCSSAssetsPlugin()
      ]
    },
  });
};

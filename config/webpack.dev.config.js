const merge = require('webpack-merge');
const webpackBaseConfig = require('./webpack.common.config');

module.exports = (env) => {
  return merge(webpackBaseConfig(env), {
    // mode: 'development',
    devtool: 'inline-source-map',
  });
};
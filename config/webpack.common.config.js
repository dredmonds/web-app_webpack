/** webpack.config.js **/ 
const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = (env) => {
  return {
    entry: {
      main: './src/index.js'
    },
    output: {
      path: path.resolve(__dirname, '../build'),
      filename: '[name].[chunkhash].js'
    },
    target: 'node',
    devServer: {
      inline: true,
      port: env.PORT,
      watchContentBase: true,
      progress: true,
      open: true,
    },
    externals: [nodeExternals()],
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader'
          }
        },
        {
          test: /\.(css|scss)$/,
          use: [
            'style-loader', 
            MiniCssExtractPlugin.loader, 
            'css-loader',
            'postcss-loader',
            'sass-loader'
          ]
        },
        {
          test: /\.(png|jpg|jpeg|gif|ico)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: './assets/images'
              }
            }
          ]
        },
        {
          test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',
            outputPath: './assets/fonts'
          }
        }
      ]
    },
    plugins: [
      new CleanWebpackPlugin(),
      new WebpackMd5Hash(),
      new webpack.DefinePlugin({
        'process.env': JSON.stringify(env)
      }),
      new MiniCssExtractPlugin({
        filename: 'style.[chunkhash].css'
      }),
      new HtmlWebpackPlugin({
        inject: false,
        hash: true,
        title: `${env.npm_package_name} v${env.npm_package_version}`,
        template: './src/index.html',
        filename: './index.html'
      }),
      new CopyWebpackPlugin([{
        from: './public',
        to: './assets'
      }])
    ]
  };
};
